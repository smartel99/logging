/**
 * @file    logger.h
 * @author  Paul Thomas
 * @date    2023-12-04
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef LOGGER_H
#define LOGGER_H

#include <cstdarg>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

#include "level.h"
#include "sink.h"

namespace Logging {
class Logger {
 private:
  static std::unordered_map<std::string, Logger> s_loggers;  // NOLINT(*-dynamic-static-initializers)

 public:
  static Logger root;  // NOLINT(*-dynamic-static-initializers)

 private:
  static std::function<uint32_t(void)> s_getTime;
  std::vector<std::unique_ptr<Sink>>   m_sinks;
  std::optional<Level>                 m_level;
  std::string                          m_name;
  explicit Logger(std::string name);

 public:
  static Logger* open(const std::string& name);
  static void    setGetTime(const std::function<uint32_t(void)>& getTime);

  template <typename T, typename... Args>
    requires std::derived_from<T, Sink>
  Logger& addSink(Args... args) {
    m_sinks.push_back(std::make_unique<T>(args...));
    return *this;
  }
  Logger& clearSinks();
  Logger& setLevel(Level level);
  Logger& clearLevel();

  template <typename... Args>
  void write(Level level, const char* fmt, Args... args) {
    if (m_level.value_or(root.m_level.value_or(Level::none)) < level) return;
    static constexpr size_t max_length = 500;
    char                    buffer[max_length];
    size_t                  length = snprintf(buffer, max_length, fmt, args...);
    toSinks(level, buffer, length);
  }
  void write(Level level, const char* fmt) { write(level, "%s", fmt); }

  template <typename... Args>
  void log(Level level, const char* fmt, Args... args) {
    if (m_level.value_or(root.m_level.value_or(Level::none)) < level) return;
    static constexpr const char* preFmt        = "%s (%05lu) [%s] %s\r\n";
    static constexpr size_t      preMaxLength  = 100;
    static constexpr size_t      postMaxLength = 500;
    char                         preBuffer[preMaxLength];
    char                         postBuffer[postMaxLength];
    snprintf(preBuffer, preMaxLength, preFmt, levelToChar(level), s_getTime(), m_name.c_str(), fmt);
    std::size_t postLength = snprintf(postBuffer, postMaxLength, preBuffer, args...);
    if (postLength > postMaxLength) {
      postBuffer[postMaxLength - 5] = '.';
      postBuffer[postMaxLength - 4] = '.';
      postBuffer[postMaxLength - 3] = '.';
      postBuffer[postMaxLength - 2] = '\r';
      postBuffer[postMaxLength - 1] = '\n';
      postLength                    = postMaxLength;
    }
    toSinks(level, postBuffer, postLength);
  }
  void log(Level level, const char* fmt) { log(level, "%s", fmt); }

  template <typename... Args>
  void e(const char* fmt, Args... args) {
    log(Level::error, fmt, args...);
  }
  void e(const char* fmt) { log(Level::error, "%s", fmt); }

  template <typename... Args>
  void w(const char* fmt, Args... args) {
    log(Level::warning, fmt, args...);
  }
  void w(const char* fmt) { log(Level::warning, "%s", fmt); }

  template <typename... Args>
  void i(const char* fmt, Args... args) {
    log(Level::info, fmt, args...);
  }
  void i(const char* fmt) { log(Level::info, "%s", fmt); }

  template <typename... Args>
  void d(const char* fmt, Args... args) {
    log(Level::debug, fmt, args...);
  }
  void d(const char* fmt) { log(Level::debug, "%s", fmt); }

  template <typename... Args>
  void t(const char* fmt, Args... args) {
    log(Level::trace, fmt, args...);
  }
  void t(const char* fmt) { log(Level::trace, "%s", fmt); }

 private:
  void toSinks(Level level, const char* buffer, size_t length);
};
}  // namespace Logging

#endif  // LOGGER_H
