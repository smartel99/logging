/**
 * @file    logger.cpp
 * @author  Paul Thomas
 * @date    2023-12-04
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "logger.h"

#include <utility>

namespace Logging {
std::function<uint32_t(void)>           Logger::s_getTime = [] -> uint32_t { return 0; };
std::unordered_map<std::string, Logger> Logger::s_loggers = std::unordered_map<std::string, Logger>();
Logger                                  Logger::root      = Logger("ROOT");

Logger::Logger(std::string name) : m_name(std::move(name)) { }

Logger *Logger::open(const std::string &name) {
  if (!s_loggers.contains(name)) { s_loggers.emplace(name, Logger(name)); }
  return &s_loggers.at(name);
}

void Logger::setGetTime(const std::function<uint32_t(void)> &getTime) { s_getTime = getTime; }

Logger &Logger::clearSinks() {
  m_sinks.clear();
  return *this;
}

Logger &Logger::setLevel(Logging::Level level) {
  m_level = level;
  return *this;
}

Logger &Logger::clearLevel() {
  m_level.reset();
  return *this;
}

void Logger::toSinks(Level level, const char *buffer, size_t length) {
  for (const auto &m_sink : m_sinks.empty() ? root.m_sinks : m_sinks) { m_sink->onWrite(level, buffer, length); }
}

}  // namespace Logging
